import React, { Component } from 'react'
import ListShoe from './ListShoe';
import { dataShoe } from './dataShoe';
import DetailShoe from './DetailShoe';
import Cart from './Cart';

export default class ShoeShop_Redux extends Component {


    render() {
        return (
            <div className='container'>
                <Cart />
                <ListShoe />
                <DetailShoe />
            </div>
        )
    }
}
