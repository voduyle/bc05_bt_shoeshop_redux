import React, { Component } from 'react'
import { connect } from 'react-redux';
import { dataShoe } from './dataShoe'
import ItemShoe from './ItemShoe'

class ListShoe extends Component {
    renderListShoe = () => {
        return this.props.list.map((item) => {
            return <div className='col-3'>
                <ItemShoe data={item} />
            </div>
        });
    }
    render() {
        return (
            <div className='row'>
                {this.renderListShoe()}
            </div>
        )
    }
}
let mapStateToProps = (state) => {
    return {
        list: state.shoeReducer.shoeArr,
    }
}
export default connect(mapStateToProps)(ListShoe);
